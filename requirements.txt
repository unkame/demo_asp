dotnet add package Microsoft.EntityFrameworkCore.Design --version 6.0.1
dotnet add package Microsoft.EntityFrameworkCore.SqlServer --version 6.0.1

dotnet tool uninstall --global dotnet-ef
dotnet tool install --global dotnet-ef --version 6.0.1

dotnet tool uninstall --global Microsoft.Web.LibraryManager.Cli
dotnet tool install --global Microsoft.Web.LibraryManager.Cli --version 2.1.161

libman init -p cdnjs
libman install twitter-bootstrap@5.1.3 -d wwwroot/lib/twitter-bootstrap

dotnet add package Microsoft.AspNetCore.Mvc.NewtonsoftJson --version 6.0.2



========= console cmd

rem dotnet ef migrations add Initial
rem dotnet ef database update
rem dotnet ef database drop --force

# Invoke-RestMethod http://localhost:5000/api/products -Method POST -Body (@{ Name="Swimming Goggles";Price=12.75; CategoryId=1; SupplierId=1} | ConvertTo-Json) -ContentType "application/json"

# Invoke-RestMethod http://localhost:5000/api/products -Method PUT -Body (@{ ProductId=1; Name="Green Kayak"; Price=275; CategoryId=1; SupplierId=1} | ConvertTo-Json) -ContentType "application/json"

# Invoke-RestMethod http://localhost:5000/api/products/2 -Method DELETE

# Invoke-RestMethod http://localhost:5000/api/products -Method POST -Body (@{ ProductId=100; Name="Swim Buoy"; Price=19.99; CategoryId=1; SupplierId=1} | ConvertTo-Json) -ContentType "application/json"

# Invoke-WebRequest http://localhost:5000/api/products/1000 |Select-Object StatusCode

# Invoke-WebRequest http://localhost:5000/api/products -Method POST -Body (@{Name="Boot Laces"} | ConvertTo-Json) -ContentType "application/json"

# Invoke-RestMethod http://localhost:5000/api/suppliers/1 -Method PATCH -ContentType "application/json" -Body '[{"op":"replace","path":"City","value":"Los Angeles"}]'

# Invoke-WebRequest http://localhost:5000/api/content/object | select @{n='Content-Type';e={ $_.Headers."Content-Type" }}, Content

# Invoke-WebRequest http://localhost:5000/api/content/object -Headers @{Accept="application/xml,*/*;q=0.8"} |select @{n='Content-Type';e={ $_.Headers."Content-Type" }}, Content

# Invoke-WebRequest http://localhost:5000/api/content/object -Headers @{Accept="img/png"} | select @{n='Content-Type';e={ $_.Headers."Content-Type" }}, Content




